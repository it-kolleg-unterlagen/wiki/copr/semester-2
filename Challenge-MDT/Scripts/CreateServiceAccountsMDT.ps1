﻿# CreateServiceAccountsMDT.ps1
$Pass = ConvertTo-SecureString "Password1!" -AsPlainText -Force
New-ADUser -Name MDT_BA -UserPrincipalName MDT_BA `
	-Description "MDT Build Account" `
	-AccountPassword $Pass `
	-ChangePasswordAtLogon $false `
	-Enabled $true `
	-Path "OU=Dienstkonten,OU=Konten,OU=Contoso,DC=contoso,DC=int" `
	-CannotChangePassword $true `
	-PasswordNeverExpires $true
New-ADUser -Name MDT_JD -UserPrincipalName MDT_JD `
	-Description "MDT Join Domain Account" `
	-AccountPassword $Pass `
	-ChangePasswordAtLogon $false `
	-Enabled $true `
	-Path "OU=Dienstkonten,OU=Konten,OU=Contoso,DC=contoso,DC=int" `
	-CannotChangePassword $true `
	-PasswordNeverExpires $true