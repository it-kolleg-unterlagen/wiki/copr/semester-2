﻿# CreateNewTaskSequence.ps1
# https://technet.microsoft.com/en-us/library/dn781089.aspx#CreateTaskSequence
Add-PSSnapIn Microsoft.BDD.PSSnapIn -ErrorAction SilentlyContinue 
Get-PSDrive -PSProvider Microsoft.BDD.PSSnapIn\MDTProvider  | Remove-PSDrive | Remove-MDTPersistentDrive
New-PSDrive -Name "DS001" -PSProvider MDTProvider `
-Root "$InstallDrive\MDTProdLab" `
-Description "MDT ProdLab" `
-NetworkPath "\\$MDT01\MDTProdLab$"
Import-MDTTaskSequence -path "DS001:\Task Sequences" `
-Name "Win-10-x64-Edu-Task-10" `
-Template "Client.xml" `
-Comments "This will deploy Windows 10 to all the desks” `
-ID "10" `
-Version “1.0” `
-OperatingSystemPath “DS001:\Operating Systems\Windows 10 Education in W10x64-EDU install.wim" `
-FullName "Windows User" `
-OrgName "Contoso" `
-HomePage "about:blank" `
-AdminPassword "Password1!" `
-Verbose