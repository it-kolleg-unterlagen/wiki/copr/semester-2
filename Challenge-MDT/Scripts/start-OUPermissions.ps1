﻿#region Benutzer Script OU-Permissions.ps1 
# Autor: Heather Poulsen - https://gallery.technet.microsoft.com/Configure-permissions-in-2326651a
#        Configure permissions in Active Directory for Windows deployment account

Set-ExecutionPolicy -ExecutionPolicy Bypass -Force

Set-Location 'C:\MDTSources\Configure permissions in Active Directory for Windows deployment account'

.\Set-OUPermissions.ps1 `
    -Account MDT_JD -TargetOU "OU=Arbeitsstationen,OU=Computer,OU=Contoso"

#endregion

