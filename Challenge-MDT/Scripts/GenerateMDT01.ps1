﻿# GenerateMDT01.ps1: Virtuellen Server MDT01 in Hyper-V erzeugen
Set-ExecutionPolicy Bypass -Force

# Enhanced session mode enables you to copy and paste the commands from the Hyper-V host to VMs, between VMs, and between RDP sessions. 
Set-VMhost -EnableEnhancedSessionMode $True

# Erstellen der virtuellen Instanz DC01 mit 2 GB RAM und zwei virtuelle CPU-Kerne.
New-VM -Name "MDT01" -Generation 2 -NewVHDPath c:\vm\MDT01.vhdx -NewVHDSizeBytes 127GB -SwitchName internal | Set-VM -StaticMemory -MemoryStartupBytes 4096MB -ProcessorCount 2 -AutomaticStartAction Nothing
	
# Aktivieren der Gastdienstschnittstelle für virtuelle Instanz MDT01 (gilt nur für deutsche Sprachvariante!)
Enable-VMIntegrationService -Name "Gastdienstschnittstelle" -VMName MDT01
	
# Pfad zur Windows Server 2016 ISO-Datei - der Pfad muss angepasst werden!
Add-VMDvdDrive -VMName MDT01 -Path C:\ISO\WindowServer2016.iso
	
# Bootreihenfolge umstellen auf DVD mit ISO
$dvd_drive = Get-VMDvdDrive -VMName MDT01
Set-VMFirmware -VMName MDT01 -FirstBootDevice $dvd_drive