﻿# IPv4DC01.ps1
New-NetIPAddress –InterfaceAlias Ethernet –IPAddress 10.0.0.1 –PrefixLength 24
Set-DnsClientServerAddress -InterfaceAlias Ethernet -ServerAddresses 10.0.0.1