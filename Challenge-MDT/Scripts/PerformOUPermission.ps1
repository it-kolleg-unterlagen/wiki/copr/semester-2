﻿# PerformOUPermission.ps1
Set-ExecutionPolicy -ExecutionPolicy Bypass -Force
Set-Location 'C:\MDTSources\Configure permissions in `
	Active Directory for Windows deployment account'
.\Set-OUPermissions.ps1 `
	-Account MDT_JD `
	-TargetOU "OU=Arbeitsstationen,OU=Computer,OU=Contoso"