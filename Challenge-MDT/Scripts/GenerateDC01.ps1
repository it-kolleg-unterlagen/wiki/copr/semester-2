﻿# GenerateDC01.ps1: Virtuellen Server DC01 in Hyper-V erzeugen
Set-ExecutionPolicy Bypass -Force

# Erstellen der virtuellen Instanz DC01 mit 2 GB RAM und zwei virtuelle CPU-Kerne.
New-VM -Name "DC01" -Generation 2 -NewVHDPath c:\vm\dc01.vhdx -NewVHDSizeBytes 60GB -SwitchName internal | Set-VM -StaticMemory -MemoryStartupBytes 2048MB -ProcessorCount 2 -AutomaticStartAction Nothing

# Aktivieren der Gastdienstschnittstelle für virtuelle Instanz DC01 (gilt nur für deutsche Sprachvariante!)
Enable-VMIntegrationService -Name "Gastdienstschnittstelle" -VMName DC01

# Pfad zur Windows Server 2016 ISO-Datei - der Pfad muss angepasst werden!
Add-VMDvdDrive -VMName DC01 -Path C:\ISO\WindowServer2016.iso

# Bootreihenfolge umstellen auf DVD mit ISO
$dvd_drive = Get-VMDvdDrive -VMName DC01
Set-VMFirmware -VMName DC01 -FirstBootDevice $dvd_drive