﻿# Autor:      Alexander Scharmer
# Purpose:    Install and configure a customized Virtualization-Instance
# Date:       2016-12-3

# write-host “Creation of VM  initiated” -foreground green
Write-Host "Choose an Instance Name" -ForegroundColor Green
$pr = (Read-Host "Instance Name (ex. SRV02)").ToUpper()
Write-Host $pr  "selected." -ForegroundColor Green
New-VM -Generation 2 –Name $pr –MemoryStartupBytes 2GB -Path "C:\HYPERV\$pr" -SwitchName 'internal'
Set-VMProcessor -VMName $pr -Count 2
Set-VM -Name $pr -AutomaticStopAction ShutDown
Set-VM -Name $pr -AutomaticStartAction Nothing

# Add Harddisk
New-VHD –Path C:\HYPERV\vDISKS\$pr.vhdx -Dynamic –BlockSizeBytes 64MB –LogicalSectorSize 4KB –SizeBytes 127GB
Add-VMHardDiskDrive -VMName $pr -Path "C:\HYPERV\vDISKS\$pr.vhdx"
$vhd = Get-VMHardDiskDrive -VMName $pr
Set-VMFirmware -VMName $pr -FirstBootDevice $vhd
# Add-VMNetworkAdapter -VMName $pr -SwitchName 'internal'
