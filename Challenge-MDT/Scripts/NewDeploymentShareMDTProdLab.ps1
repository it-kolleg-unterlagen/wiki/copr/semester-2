﻿# NewDeploymentShareMDTProdLab.ps1
Add-PSSnapin -Name Microsoft.BDD.PSSnapIn  -ErrorAction SilentlyContinue 
Start-Sleep -Milliseconds 1000
$MDTServer = (get-wmiobject win32_computersystem).Name
$InstallDrive = "C:"
New-Item -Path $InstallDrive\MDTProdLab -ItemType directory
New-PSDrive -Name "DS001" -PSProvider MDTProvider -Root "$InstallDrive\MDTProdLab" -Description "MDT ProdLab" -NetworkPath "\\$MDTServer\MDTProdLab$"  -Verbose | add-MDTPersistentDrive
New-SmbShare -Name MDTProdLab$ -Path "$InstallDrive\MDTProdLab" -ChangeAccess JEDER