﻿# https://technet.microsoft.com/en-us/itpro/powershell/windows/hyper-v/copy-vmfile

# .NET-Library um Dateien und Ordner als ZIP-Datei generieren zu lassen
[Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem")
$Compression = [System.IO.Compression.CompressionLevel]::Optimal
$IncludeBaseDirectory = $false

# Pfade anpassen
$Source = "C:\MDTSources"
$Destination = "C:\MDT\MDTSources.zip"
New-Item -Path C:\MDT -ItemType Directory -Force -ErrorAction SilentlyContinue
Remove-Item -Path $Destination -Force -ErrorAction SilentlyContinue

# Los - zippe!
[System.IO.Compression.ZipFile]::CreateFromDirectory($Source,$Destination,$Compression,$IncludeBaseDirectory)

# Warte kurz ...
Start-Sleep -Milliseconds 1000

# ... und kopiere die ZIP-Datei in die virtuelle Maschine (die Aktivierung der Gastdienste ist erforderlich).
Copy-VMFile MDT01 -SourcePath $Destination -DestinationPath "C:\MDT\MDTSources.zip" -CreateFullPath -FileSource Host

