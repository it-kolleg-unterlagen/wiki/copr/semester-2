Set-ExecutionPolicy unrestricted -Force

$VMLocation = "c:\VM"
$VMISO = "C:\MDTSource\HydrationSALE.iso"
$VMNetwork = "vSwitchNAT"

if (!(Test-Path -Path $VMLocation)) {
    New-Item -path $VMLocation -ItemType Directory  }
 else{ Remove-Item -Path $VMLocation -Recurse -force -ErrorAction SilentlyContinue }


# Create DC01
$VMName = "DC01"
$VMMemory = 2048MB
$VMDiskSize = 60GB
New-VM -Name $VMName -Generation 2 -BootDevice CD -MemoryStartupBytes $VMMemory -SwitchName $VMNetwork -Path $VMLocation -NoVHD -Verbose | Set-VM -StaticMemory -ProcessorCount 2 -AutomaticStartAction Nothing
New-VHD -Path "$VMLocation\$VMName\Virtual Hard Disks\$VMName-Disk1.vhdx" -SizeBytes $VMDiskSize -Verbose
Add-VMHardDiskDrive -VMName $VMName -Path "$VMLocation\$VMName\Virtual Hard Disks\$VMName-Disk1.vhdx" -Verbose
Set-VMDvdDrive -VMName $VMName -Path $VMISO -Verbose
# Start-VM -Name $VMName
VMConnect localhost $VMName

# Create MDT01
$VMName = "MDT01"
$VMMemory = 4096MB
$VMDiskSize = 127GB
New-VM -Name $VMName -Generation 2 -BootDevice CD -MemoryStartupBytes $VMMemory -SwitchName $VMNetwork -Path $VMLocation -NoVHD -Verbose | Set-VM -StaticMemory -ProcessorCount 2 -AutomaticStartAction Nothing
New-VHD -Path "$VMLocation\$VMName\Virtual Hard Disks\$VMName-Disk1.vhdx" -SizeBytes $VMDiskSize -Verbose
Add-VMHardDiskDrive -VMName $VMName -Path "$VMLocation\$VMName\Virtual Hard Disks\$VMName-Disk1.vhdx" -Verbose
Set-VMDvdDrive -VMName $VMName -Path $VMISO -Verbose
VMConnect localhost $VMName