﻿# ConfigSQLExpressMDT.ps1
Add-PSSnapIn Microsoft.BDD.PSSnapIn -ErrorAction SilentlyContinue 
Get-PSDrive -PSProvider Microsoft.BDD.PSSnapIn\MDTProvider  | Remove-PSDrive | Remove-MDTPersistentDrive
New-PSDrive -Name "DS001" -PSProvider MDTProvider `
  -Root "$InstallDrive\MDTProdLab" `
  -Description "MDT ProdLab" `
  -NetworkPath "\\$MDT01\MDTProdLab$"

# How to: Enable or Disable a Server Network Protocol (SQL Server PowerShell)
# https://technet.microsoft.com/en-us/library/dd206997(v=sql.105).aspx
[reflection.assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo")
[reflection.assembly]::LoadWithPartialName("Microsoft.SqlServer.SqlWmiManagement")
$smo = 'Microsoft.SqlServer.Management.Smo.'
$wmi = new-object ($smo + 'Wmi.ManagedComputer').
# List the object properties, including the instance names.
$Wmi
# Disable the TCP protocol on the default instance.
# $uri = "ManagedComputer[@Name='$MDTServer']/ ServerInstance[@Name='ADK']/ServerProtocol[@Name='Tcp']"
$uri = "ManagedComputer[@Name='" + (get-item env:\computername).Value + "']/ServerInstance[@Name='ADK']/ServerProtocol[@Name='Tcp']"
$Tcp = $wmi.GetSmoObject($uri)
$Tcp.IsEnabled = $false
$Tcp.Alter()
$Tcp
# Enable the named pipes protocol for the default instance.
$uri = "ManagedComputer[@Name='" + (get-item env:\computername).Value + "']/ServerInstance[@Name='ADK']/ServerProtocol[@Name='Np']"
# $uri = "ManagedComputer[@Name='$MDTServer']/ ServerInstance[@Name='ADK']/ServerProtocol[@Name='Np']"
$Np = $wmi.GetSmoObject($uri)
$Np.IsEnabled = $true
$Np.Alter()
$Np

Start-Sleep -Milliseconds 1000

Start-Process sc -ArgumentList {config sqlbrowser start= auto} -ErrorAction SilentlyContinue
Start-Service "SQL Server Browser" -ErrorAction SilentlyContinue
Stop-Service "SQL Server (ADK)"  -ErrorAction SilentlyContinue
Start-Service "SQL Server (ADK)" -ErrorAction SilentlyContinue

Start-Sleep -Milliseconds 1000

# Add a new Database
$MDTServer = (get-wmiobject win32_computersystem).Name
New-MDTDatabase -SQLServer $MDTServer -Database "DB-Prod-Lab" -Instance "ADK" -SQLShare "MDTProdLab$" -Path DS001: -Force 