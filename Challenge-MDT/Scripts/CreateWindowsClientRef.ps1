﻿## Parent Script by http://deploymentresearch.com/Research/Post/578/Building-the-perfect-Windows-Server-2016-reference-image
## <!-- Modified by Alexander Scharmer (SALE) !>
## // CLIENT-IMAGE Version 1.3 - 29.04.2017

# Check for elevation
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    Write-Warning "Oupps, you need to run this script from an elevated PowerShell prompt!`nPlease start the PowerShell prompt as an Administrator and re-run the script."
    Write-Warning "Aborting script..."
    Throw
}


$ISO = "D:\alexhome\Client\ISO\SW_DVD5_WIN_EDU_10_1607_64BIT_German_MLF_X21-07188.ISO"
$ServiceStack = "D:\alexhome\Client\msu\1.0Servicestack-Update for Windows 10 Version 1607 for x64-based Systems (KB4013418)\windows10.0-kb4013418-x64_b7165b95be791aeb025135efe60e00db2800c5c6.msu"
$AdobeFlashUpdate = "D:\alexhome\Client\msu\2.0Security Update for Adobe Flash Player for Windows 10 Version 1607 (for x64-based Systems) (KB4018483)\windows10.0-kb4018483-x64_ebbd55a721bb98d665b2f4cc17971aa797701971.msu"
$Latestdefinitionupdates = "D:\alexhome\Client\msu\3.0Update for Windows 10 Version 1607 for x64-based Systems (KB3150513)\windows10.0-kb3150513-x64_eeda0f695d57bea9b054eb8cfebb00e2cd87b112.msu"
$DynamicUpdate = "D:\alexhome\Client\msu\4.0Dynamic Update KB4013420\windows10.0-kb4013420-x64_ee75611328d74d2801bc423c720c46ab86c98a90.cab"
$CU = "D:\alexhome\Client\msu\Cumulative Update for Windows 10 Version 1607 for x64-based Systems (KB4015217)\windows10.0-kb4015217-x64_60bfcc7b365f9ab40608e2fb96bc2be8229bc319.msu"
$MountFolder = "D:\alexhome\Client\Mount"
$RefImage = "D:\alexhome\Client\WIM\install.wim"

 
# Verify that the ISO and CU files existnote
if (!(Test-Path -path $ISO)) {Write-Warning "Could not find Windows Client ISO file. Aborting...";Break}
if (!(Test-Path -path $CU)) {Write-Warning "Cumulative Update for Windows Client. Aborting...";Break}

# Delete RefImage-File before continue.
if (Test-Path -path $RefImage) {Write-Warning "RefImage exists. Delete it.";Remove-Item $RefImage -Force}

 
# Mount the Windows ISO
Mount-DiskImage -ImagePath $ISO
$ISOImage = Get-DiskImage -ImagePath $ISO | Get-Volume
$ISODrive = [string]$ISOImage.DriveLetter+":"
 
# Extract the Windows Client Standard index to a new WIM
Export-WindowsImage -SourceImagePath "$ISODrive\Sources\install.wim" -SourceName "Windows 10 Education" -DestinationImagePath $RefImage

# Add the CU to the Windows Client Standardimage
if (!(Test-Path -path $MountFolder)) {New-Item -path $MountFolder -ItemType Directory}
Mount-WindowsImage -ImagePath $RefImage -Index 1 -Path $MountFolder 

# Add .NET Framework 3.5.1 to the Windows Client Standard image
Add-WindowsPackage -PackagePath $ISODrive\sources\sxs\microsoft-windows-netfx3-ondemand-package.cab -Path $MountFolder

# Activate/deactivate Features
Enable-WindowsOptionalFeature -FeatureName "DirectPlay" -Path $MountFolder -LimitAccess -All
Disable-WindowsOptionalFeature -FeatureName "SMB1Protocol" -Path $MountFolder


Add-WindowsPackage -PackagePath $ServiceStack -Path $MountFolder
Add-WindowsPackage -PackagePath $AdobeFlashUpdate -Path $MountFolder
Add-WindowsPackage -PackagePath $Latestdefinitionupdates -Path $MountFolder
Add-WindowsPackage -PackagePath $CU -Path $MountFolder



 

# install drivers
Add-WindowsDriver –Path $MountFolder –Driver D:\alexhome\Client\drivers –Recurse   # installs drivers in specified directory and subdirectories


# Set Default App Associations
# Dism.exe /Image:$MountFolder /Import-DefaultAppAssociations:'C:\client\win1064\$oem$\$1\apps\DefaultApp.xml'


# Settings to apply
$RemoteDesktopEnable = $True # Enabling Remote Desktop

$hive  = Start-Process -FilePath "REG.EXE" -ArgumentList @("LOAD HKLM\REMOTE", (Join-Path $MountFolder "Windows\System32\Config\System")) -NoNewWindow
            Start-Sleep -s 3        
                        if ( $RemoteDesktopEnable -eq $True ) {

                            
                            Set-ItemProperty -Path "HKLM:\REMOTE\ControlSet001\Control\Terminal Server" -Name "fDenyTSConnections" -Value 0

                        }


            Start-Sleep -s 2
         Start-Process -FilePath "REG.EXE" -ArgumentList @("UNLOAD HKLM\REMOTE") -NoNewWindow

# Build Name for ISO-Output-File
$Imagename = [string](Get-WindowsImage -ImagePath $RefImage -Index 1).ImageName + "_" + (Get-WindowsImage -ImagePath $RefImage -Index 1).Languages + "_" +  (Get-WindowsImage -ImagePath $RefImage -Index 1).Version + "1066" + ".iso"
$Imagename = [string]$Imagename.Replace(" ","")


# Start cleanup of the image
# By default, non-major updates (such as ZDPs, or LCUs) are not restored. To ensure that updates preinstalled during manufacturing are not discarded after recovery, 
# they should be marked as permanent by using the /Cleanup-Image command in DISM with the /StartComponentCleanup and /ResetBase options. Updates marked as permanent are always restored during recovery.
#dism  /Image:C:\client\Mount  /Cleanup-Image /StartComponentCleanup /resetbase /scratchdir:c:\client\scratchdir
 
# Dismount the Windows Standard image
DisMount-WindowsImage -Path $MountFolder -save # -CheckIntegrity or -Discard
# DisMount-WindowsImage -Path $MountFolder -Discard
# Clear-WindowsCorruptMountPoint
 
# Dismount the Windows ISO
Dismount-DiskImage -ImagePath $ISO -Verbose


# Create a bootable ISO. The script assumes you installed Windows ADK 10 in the default location.
  
$MediaFolder = 'D:\alexhome\Client\Media'
$DesktopPath = [Environment]::GetFolderPath("Desktop")
$ISOFile = [string]"$DesktopPath\$Imagename"
$ToolsPath = "C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg"

# replace modified wim file within media folder
Copy-Item -Path $RefImage -Destination "$MediaFolder\sources" -Force -PassThru

 
Write-Output "Checking for oscdimg.exe"
If (Test-Path $ToolsPath\oscdimg.exe){
    Write-Output "Oscdimg.exe found, OK, continuing..."
    Write-Output ""
    }
Else {
    Write-Warning "Oupps, cannot find Oscdimg.exe. Make sure it is copied to $ToolsPath. Aborting"
    Break
}
 
Write-Output "Checking for Media folder"
If (Test-Path $MediaFolder\setup.exe){
    Write-Output "Installation media found, OK, continuing..."
    Write-Output ""
    }
Else {
    Write-Warning "Oupps, cannot find Installation media. Make sure it is copied to $MediaFolder. Aborting"
    Break
}
 
# Write-Output "Checking for ISO folder"
# If (Test-Path $ISO){
 #   Write-Output "ISO folder found, OK, continuing..."
 #   Write-Output "
 #   }
#Else {
 #   Write-Warning "ISO folder not found, creating $ISO folder"
 #   New-Item $ISO -ItemType Directory
#}
 
  
# Create the Windows ISO
$BootData='2#p0,e,b"{0}"#pEF,e,b"{1}"' -f "$ToolsPath\etfsboot.com","$ToolsPath\efisys_noprompt.bin"
  
$Proc = Start-Process -FilePath "$ToolsPath\oscdimg.exe" -ArgumentList @("-bootdata:$BootData",'-u2','-udfver102',"$MediaFolder","$ISOFile") -PassThru -Wait -NoNewWindow
if($Proc.ExitCode -ne 0)
{
    Throw "Failed to generate ISO with exitcode: $($Proc.ExitCode)"
}
else {


    Write-Host "Media creation successfully done!" -ForegroundColor Green


}