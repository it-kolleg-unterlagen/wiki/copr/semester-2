﻿Mount-WindowsImage -Path C:\Media\Mount -ImagePath "C:\HydrationCMWS2016\DS\Operating Systems\WS2016\REFWS2016-001.wim" -Index 1

$Latestdefinitionupdates = "D:\alexhome\Server\MSU\windows10.0-kb3150513-v4-x64_0bc267c99701556f97cad697c15690671275b2b1.msu"
$ServiceStack = "D:\alexhome\Server\MSU\windows10.0-kb4013418-x64_b7165b95be791aeb025135efe60e00db2800c5c6.msu"
$CU = "D:\alexhome\Server\MSU\windows10.0-kb4023680-x64_de5502ce899738bedd5eb20f10cfe67ea26ff5b6.msu"

Add-WindowsPackage -PackagePath $ServiceStack -Path C:\Media\Mount
Add-WindowsPackage -PackagePath $Latestdefinitionupdates -Path C:\Media\Mount
Add-WindowsPackage -PackagePath $CU -Path C:\Media\Mount

if (!(Test-Path -Path C:\Media\Mount\MDTSources\Scripts)) {
New-Item C:\Media\Mount\MDTSources\Scripts -ItemType Directory 
}

### Copy-Item -path 'C:\HydrationCMWS2016\DS\$OEM$\$1\Test\*' -Destination C:\Media\Mount\MDTSources\Scripts -Recurse

Dismount-WindowsImage -Path C:\Media\Mount -save