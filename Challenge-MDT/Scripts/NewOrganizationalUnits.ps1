﻿# NewOrganizationalUnits.ps1
New-ADOrganizationalUnit -Name "Contoso"
New-ADOrganizationalUnit -Path "OU=Contoso,DC=contoso,DC=int" `
	-Name "Konten"
New-ADOrganizationalUnit -Path "OU=Contoso,DC=contoso,DC=int" `
	-Name "Computer"
New-ADOrganizationalUnit -Path "OU=Contoso,DC=contoso,DC=int" `
	-Name "Gruppen"
New-ADOrganizationalUnit -Path "OU=Konten,OU=Contoso,DC=contoso,DC=int" `
	-Name "Administratoren"
New-ADOrganizationalUnit -Path "OU=Konten,OU=Contoso,DC=contoso,DC=int" `
	-Name "Dienstkonten"
New-ADOrganizationalUnit -Path "OU=Konten,OU=Contoso,DC=contoso,DC=int" `
	-Name "Benutzer"
New-ADOrganizationalUnit -Path "OU=Computer,OU=Contoso,DC=contoso,DC=int" `
	-Name "Server"
New-ADOrganizationalUnit -Path "OU=Computer,OU=Contoso,DC=contoso,DC=int" `
	-Name "Arbeitsstationen"
New-ADOrganizationalUnit -Path "OU=Gruppen,OU=Contoso,DC=contoso,DC=int" `
	-Name "Sicherheitsgruppen"