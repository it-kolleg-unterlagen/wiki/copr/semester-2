﻿# DomainJoinMDT01.ps1
$pass = "Password1!" | ConvertTo-SecureString -AsPlainText -Force
$user = "contoso\administrator"
$cred = New-Object System.Management.Automation.PSCredential($user,$pass)
Add-Computer -DomainName contoso.int -Credential $cred
Restart-Computer
