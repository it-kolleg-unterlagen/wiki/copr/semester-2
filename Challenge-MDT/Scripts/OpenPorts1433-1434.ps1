﻿# OpenPorts1433-1434.ps1
# Create Rules: Open Ports 1433 TCP and 1434 UDP in Windows-Firewall
New-NetFirewallRule -DisplayName "MSSQL TCP 1433" `
	-Direction Inbound -LocalPort 1433 -Protocol TCP `
	-Action Allow
New-NetFirewallRule -DisplayName "MSSQL UDP 1434" `
	-Direction Inbound -LocalPort 1434 -Protocol UDP `
	-Action Allow