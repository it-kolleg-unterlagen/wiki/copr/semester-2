﻿<# 

Configure Hyper-V
https://technet.microsoft.com/en-us/itpro/windows/deploy/windows-10-poc#lab-setup
#>

Set-ExecutionPolicy Unrestricted –Force 

New-VMSwitch -Name poc-internal -SwitchType Internal -Notes "PoC Network"
New-VMSwitch -Name poc-external -NetAdapterName (Get-NetAdapter |?{$_.Status -eq "Up" -and !$_.Virtual}).Name -Notes "PoC External"

# Virtuelle Server 
$maxRAM = (Get-VMHostNumaNode).MemoryAvailable / 4
New-VM -Name "DC01" -VHDPath c:\vhd\2016-dc01-1.vhdx -SwitchName poc-internal
# Set-VMMemory -VMName "DC1" -DynamicMemoryEnabled $true -MinimumBytes 512MB -MaximumBytes $maxRAM -Buffer 20
Enable-VMIntegrationService -Name "Guest Service Interface" -VMName DC01
New-VM -Name "MDT01" -VHDPath c:\vhd\2016-mdt01-2.vhdx -SwitchName poc-internal
Add-VMNetworkAdapter -VMName "SRV1" -SwitchName "poc-external"
# Set-VMMemory -VMName "SRV1" -DynamicMemoryEnabled $true -MinimumBytes 512MB -MaximumBytes $maxRAM -Buffer 80
Enable-VMIntegrationService -Name "Guest Service Interface" -VMName MDT01


# PC001 Client
New-VM -Name "PC001" -Generation 2 -VHDPath c:\vhd\PC1.vhdx -SwitchName poc-internal
# Set-VMMemory -VMName "PC001" -DynamicMemoryEnabled $true -MinimumBytes 512MB -MaximumBytes $maxRAM -Buffer 20
Enable-VMIntegrationService -Name "Guest Service Interface" -VMName PC001
Add-VMHardDiskDrive -VMName PC001 -Path c:\vhd\d.vhd
# Set-VMDvdDrive -VMName PC001 -Path c:\vhd\w10-enterprise.iso
# Set-VMMemory -VMName "PC1" -DynamicMemoryEnabled $true -MinimumBytes 512MB -MaximumBytes $maxRAM -Buffer 20
Start-VM PC1
vmconnect localhost PC001
 

 Rename-Computer DC01 -Restart -Force
 New-NetIPAddress –InterfaceAlias Ethernet –IPAddress 192.168.0.1 –PrefixLength 24 -DefaultGateway 192.168.0.2
 Set-DnsClientServerAddress -InterfaceAlias Ethernet -ServerAddresses 192.168.0.1,192.168.0.2
 
 Install-WindowsFeature -Name AD-Domain-Services -IncludeAllSubFeature -IncludeManagementTools
 Restart-Computer

 $pass = "Password1!" | ConvertTo-SecureString -AsPlainText -Force
 Install-ADDSForest -DomainName contoso.int -InstallDns -SafeModeAdministratorPassword $pass -Force


 Add-DnsServerPrimaryZone -NetworkID "192.168.0.0/24" -ReplicationScope Forest
 Add-WindowsFeature -Name DHCP -IncludeManagementTools
 netsh dhcp add securitygroups
 Restart-Service DHCPServer
 Add-DhcpServerInDC  dc01.contoso.int  192.168.0.1
 Set-ItemProperty –Path registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\ServerManager\Roles\12 –Name ConfigurationState –Value 2

 Add-DhcpServerv4Scope -Name "PoC Scope" -StartRange 192.168.0.100 -EndRange 192.168.0.199 -SubnetMask 255.255.255.0 -Description "Windows 10 PoC" -State Active
Set-DhcpServerv4OptionValue -ScopeId 192.168.0.0 -DnsDomain contoso.int -Router 192.168.0.2 -DnsServer 192.168.0.1,192.168.0.2 -Force



Rename-Computer SRV1
New-NetIPAddress –InterfaceAlias Ethernet –IPAddress 192.168.0.2 –PrefixLength 24
Set-DnsClientServerAddress -InterfaceAlias Ethernet -ServerAddresses 192.168.0.1,192.168.0.2
Restart-Computer

$pass = "Password1!" | ConvertTo-SecureString -AsPlainText -Force
$user = "contoso\administrator"
$cred = New-Object System.Management.Automation.PSCredential($user,$pass)
Add-Computer -DomainName contoso.int -Credential $cred
Restart-Computer

Install-WindowsFeature -Name WDS -IncludeManagementTools

#Configure service and user accounts
New-ADUser -Name User1 -UserPrincipalName user1 -Description "User account" -AccountPassword (ConvertTo-SecureString "Password1!" -AsPlainText -Force) -ChangePasswordAtLogon $false -Enabled $true
New-ADUser -Name MDT_BA -UserPrincipalName MDT_BA -Description "MDT Build Account" -AccountPassword (ConvertTo-SecureString "Password1!" -AsPlainText -Force) -ChangePasswordAtLogon $false -Enabled $true
New-ADUser -Name MDT_JD -UserPrincipalName CM_JD -Description "MDT Join Domain Account" -AccountPassword (ConvertTo-SecureString "Password1!" -AsPlainText -Force) -ChangePasswordAtLogon $false -Enabled $true
# New-ADUser -Name CM_NAA -UserPrincipalName CM_NAA -Description "Configuration Manager Network Access Account" -AccountPassword (ConvertTo-SecureString "Password1!" -AsPlainText -Force) -ChangePasswordAtLogon $false -Enabled $true
# Add-ADGroupMember "Domain Admins" MDT_BA,CM_JD,CM_NAA
# Set-ADUser -Identity user1 -PasswordNeverExpires $true
# Set-ADUser -Identity administrator -PasswordNeverExpires $true
Set-ADUser -Identity MDT_BA -PasswordNeverExpires $true
Set-ADUser -Identity MDT_JD -PasswordNeverExpires $true


#Verify the configuration
Get-Service NTDS,DNS,DHCP
 DCDiag -a
 Get-DnsServerResourceRecord -ZoneName contoso.int -RRType A
 Get-DnsServerForwarder
 Resolve-DnsName -Server dc1.contoso.int -Name www.microsoft.com
 Get-DhcpServerInDC
 Get-DhcpServerv4Statistics
 ipconfig /all




 # MDT Server
 # https://technet.microsoft.com/en-us/itpro/windows/deploy/windows-10-poc-mdt

 # disable IE Enhanced Security Configuration for Administrators
 $AdminKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
 Set-ItemProperty -Path $AdminKey -Name "IsInstalled" -Value 0
 Stop-Process -Name Explorer

If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    Write-Warning "Oupps, you need to run this script from an elevated PowerShell prompt!`nPlease start the PowerShell prompt as an Administrator and re-run the script."
    Write-Warning "Aborting script..."
    Break
}
 
# Validation OK, load the MDT SnapIn
Add-PSSnapIn Microsoft.BDD.PSSnapIn -ErrorAction SilentlyContinue 
Start-Sleep -Milliseconds 1000
 
# Create MDT deployment share
# http://deploymentresearch.com/Research/Post/304/Creating-the-MDT-2012-2013-Deployment-share-using-PowerShell
$MDTServer = (get-wmiobject win32_computersystem).Name
$InstallDrive = "C:"
New-Item -Path $InstallDrive\MDTBuildLab -ItemType directory


# Configure NTFS Permissions for the MDT Build Lab deployment share
$DeploymentShareNTFS = "$InstallDrive\MDTBuildLab"
icacls $DeploymentShareNTFS /grant '"Benutzer":(OI)(CI)(RX)'
icacls $DeploymentShareNTFS /grant '"Administratoren":(OI)(CI)(F)'
icacls $DeploymentShareNTFS /grant '"SYSTEM":(OI)(CI)(F)'
icacls "$DeploymentShareNTFS\Captures" /grant '"CONTOSO\MDT_BA":(OI)(CI)(M)'

New-PSDrive -Name "DS001" -PSProvider MDTProvider -Root "$InstallDrive\MDTBuildLab" -Description "MDT BuildLab" -NetworkPath "\\$MDTServer\MDTBuildLab$"  -Verbose | add-MDTPersistentDrive
New-SmbShare –Name MDTBuildLab$ –Path "$InstallDrive\MDTBuildLab" –ChangeAccess JEDER
New-Item -Path $InstallDrive\MDTBuildLab\Logs -ItemType directory
New-SmbShare –Name MDTBuildLabLogs$ –Path "$InstallDrive\MDTBuildLab\Logs" –ChangeAccess JEDER

# Remove-MDTPersistentDrive -Name DS001
# Remove-PSDrive -PSProvider MDTProvider -Name DS001

Start-Sleep -Milliseconds 1000

# Configure DeploymentShare
Set-ItemProperty -Path DS001: -Name SupportX86 -Value 'False'
Set-ItemProperty -Path DS001: -Name UNCPath -Value "\\$MDTServer\MDTBuildLab$"
Set-ItemProperty -Path DS001: -Name MonitorHost $MDTServer
Set-ItemProperty -Path DS001: -Name MonitorEventPort -Value '9800'
Set-ItemProperty -Path DS001: -Name MonitorDataPort -Value '9801'

# Configure x86 boot image Deployment Share Properties
Set-ItemProperty -Path DS001: -Name Boot.x86.UseBootWim -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x86.ScratchSpace -Value '32'
Set-ItemProperty -Path DS001: -Name Boot.x86.IncludeAllDrivers -Value $True
Set-ItemProperty -Path DS001: -Name Boot.x86.IncludeNetworkDrivers -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x86.IncludeMassStorageDrivers -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x86.IncludeVideoDrivers -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x86.IncludeSystemDrivers -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x86.BackgroundFile -Value '%DEPLOYROOT%\Extra\deployHG.bmp'
Set-ItemProperty -Path DS001: -Name Boot.x86.ExtraDirectory -Value '%DEPLOYROOT%\Extra'
Set-ItemProperty -Path DS001: -Name Boot.x86.GenerateGenericWIM -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x86.GenerateGenericISO -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x86.GenericWIMDescription -Value 'Generic Windows PE (x86)'
Set-ItemProperty -Path DS001: -Name Boot.x86.GenericISOName -Value 'Generic_x86.iso'
Set-ItemProperty -Path DS001: -Name Boot.x86.GenerateLiteTouchISO -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x86.LiteTouchWIMDescription -Value 'Lite Touch Windows PE (x86)'
Set-ItemProperty -Path DS001: -Name Boot.x86.LiteTouchISOName -Value 'LiteTouchPE_x86.iso'
Set-ItemProperty -Path DS001: -Name Boot.x86.SelectionProfile -Value 'Drivers_WinPE_x86'
Set-ItemProperty -Path DS001: -Name Boot.x86.SupportUEFI -Value $True
Set-ItemProperty -Path DS001: -Name Boot.x86.FeaturePacks -Value 'winpe-mdac'
 
# Configure x64 boot image Deployment Share Properties
Set-ItemProperty -Path DS001: -Name Boot.x64.UseBootWim -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x64.ScratchSpace -Value '128'
Set-ItemProperty -Path DS001: -Name Boot.x64.IncludeAllDrivers -Value $True
Set-ItemProperty -Path DS001: -Name Boot.x64.IncludeNetworkDrivers -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x64.IncludeMassStorageDrivers -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x64.IncludeVideoDrivers -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x64.IncludeSystemDrivers -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x64.BackgroundFile -Value '%DEPLOYROOT%\Extra\deployHG.bmp'
Set-ItemProperty -Path DS001: -Name Boot.x64.ExtraDirectory -Value '%DEPLOYROOT%\Extra'
Set-ItemProperty -Path DS001: -Name Boot.x64.GenerateGenericWIM -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x64.GenerateGenericISO -Value $False
Set-ItemProperty -Path DS001: -Name Boot.x64.GenericWIMDescription -Value 'Generic Windows PE (x64)'
Set-ItemProperty -Path DS001: -Name Boot.x64.GenericISOName -Value 'Generic_x64.iso'
Set-ItemProperty -Path DS001: -Name Boot.x64.GenerateLiteTouchISO -Value $True
Set-ItemProperty -Path DS001: -Name Boot.x64.LiteTouchWIMDescription -Value 'MDT Build x64'
Set-ItemProperty -Path DS001: -Name Boot.x64.LiteTouchISOName -Value 'MDTBuild_PE_x64.iso'
Set-ItemProperty -Path DS001: -Name Boot.x64.SelectionProfile -Value 'Drivers_WinPE_x64'
Set-ItemProperty -Path DS001: -Name Boot.x64.SupportUEFI -Value $True
Set-ItemProperty -Path DS001: -Name Boot.x64.FeaturePacks -Value 'winpe-mdac'
Set-ItemProperty -Path DS001: -Name Boot.x64.ScratchSpace -Value '512'

Start-Sleep -Milliseconds 1000

# Create Selection Profiles for Driver Selection
New-Item -Path "DS001:\Selection Profiles" -enable "True" -Name "Drivers_WinPE_x64" -Comments "Include all Drivers for WinPE-x64-Boot" -Definition "<SelectionProfile><Include path=`"Out-of-Box Drivers\WinPE\x64`" /></SelectionProfile>" -ReadOnly "False"
New-Item -Path "DS001:\Selection Profiles" -enable "True" -Name "Drivers_WinPE_x86" -Comments "Include all Drivers for WinPE-x86-Boot" -Definition "<SelectionProfile><Include path=`"Out-of-Box Drivers\WinPE\x86`" /></SelectionProfile>" -ReadOnly "False"
New-Item -Path "DS001:\Operating Systems" -enable "True" -Name "Windows 10" -Comments "" -ItemType "folder"
New-Item -Path "DS001:\Operating Systems\Windows 10" -enable "True" -Name "x64" -Comments "" -ItemType "folder"

Start-Sleep -Milliseconds 1000

Import-MDTOperatingSystem -Path "DS001:\Operating Systems\Windows 10\x64" -SourcePath "E:\" -DestinationFolder "Windows 10\x64" -Verbose

Start-Sleep -Milliseconds 1000

# Create WinPE x64 and x86 Folders in Out-of-Box Drivers
New-Item -Path "DS001:\Out-of-Box Drivers" -enable "True" -Name "WinPE" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Out-of-Box Drivers\WinPE" -enable "True" -Name "x64" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Out-of-Box Drivers\WinPE" -enable "True" -Name "x86" -Comments "" -ItemType "folder"
 
# Create Platform Folders in Out-of-Box Drivers Folders
New-Item -Path "DS001:\Out-of-Box Drivers" -enable "True" -Name "Windows 8.1" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Out-of-Box Drivers\Windows 8.1" -enable "True" -Name "x64" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Out-of-Box Drivers\Windows 8.1" -enable "True" -Name "x86" -Comments "" -ItemType "folder"
New-Item -Path "DS001:\Out-of-Box Drivers" -enable "True" -Name "Windows 10" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Out-of-Box Drivers\Windows 10" -enable "True" -Name "x64" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Out-of-Box Drivers\Windows 10" -enable "True" -Name "x86" -Comments "" -ItemType "folder"

# Create Sub-Folders in Packages Folders
New-Item -Path "DS001:\Packages" -enable "True" -Name "Windows 8.1" -Comments "" -ItemType "folder"
New-Item -Path "DS001:\Packages" -enable "True" -Name "Windows 10" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Packages\Windows 10" -enable "True" -Name "x64" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Packages\Windows 10" -enable "True" -Name "x86" -Comments "" -ItemType "folder"
New-Item -Path "DS001:\Packages" -enable "True" -Name "Server 2012" -Comments "" -ItemType "folder"
New-Item -Path "DS001:\Packages" -enable "True" -Name "Server 2016" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Packages\Server 2016" -enable "True" -Name "x64" -Comments "" -ItemType "folder"
New-Item -path "DS001:\Packages\Server 2016" -enable "True" -Name "x86" -Comments "" -ItemType "folder"

Start-Sleep -Milliseconds 1000

# $MDTSettings = Get-ItemProperty DS001:
# $MDTSettings | Select *


# Specify Bootstrap.ini Properties
$UserDomain = "contoso.int"
$UserID = "MDT_BA"
$UserPassword = "Password1!"

# Bootstrap.ini Properties
$BStext = @"
[Settings]
Priority=Default
 
[Default]
DeployRoot=\\$MDTServer\MDTBuildLab$
 
UserDomain=$UserDomain
UserID=$UserID
UserPassword=$UserPassword
 
KeyboardLocale=de-DE
SkipBDDWelcome=YES
"@



# Specify CustomSettings.ini Properties
# Specify Task Sequence Personalization
$SMSTSOrgName = "%TaskSequenceName% on %Model%"
$SMSTSPackageName = "by Alexander Scharmer"

# CustomSettings.ini Properties
$CStext = @"
[Settings]
Priority=Model, Default
Properties=MyCustomProperty

; Hyper-V
[Virtual Machine]
_SMSTSOrgName=$SMSTSOrgName
_SMSTSPackageName=$SMSTSPackageName
TaskSequenceID=10
OSDComputerName=REFIMG
JoinWorkgroup=WORKGROUP
; Computer Backup Location
DoCapture=YES
UserDataLocation=NONE
ComputerBackupLocation=NETWORK
BackupShare=\\$MDTServer\MDTBuildLab$ 
BackupDir=Captures
BackupFile=%TaskSequenceID%_#month(date) & "-" & day(date) & "-" & year(date)#.wim
HIDESHELL=NO
ApplyGPOPack=NO
FinishAction=SHUTDOWN
DoNotCreateExtraPartition=YES

; VMware
[VMware Virtual Platform]
_SMSTSPackageName=$SMSTSPackageName
TaskSequenceID=10
OSDComputerName=OSBUILD
JoinWorkgroup=BUILD
; Computer Backup Location
DoCapture=YES
ComputerBackupLocation=$ComputerBackupLocation
BackupFile=$BackupFile
; Finish Action
FinishAction=SHUTDOWN

[Default]
_SMSTSOrgName=$SMSTSOrgName
_SMSTSPackageName=$SMSTSPackageName
OSInstall=YES
SkipAdminPassword=YES
SkipBitLocker=YES
SkipCapture=NO
SkipComputerBackup=YES
SkipComputerName=YES
OSDComputerName=%SerialNumber%
SkipDomainMembership=YES
SkipLocaleSelection=YES
SkipProductKey=YES
SkipRoles=YES
SkipSummary=YES
SkipTaskSequence=YES
TaskSequenceID=10
SkipTimeZone=YES
SkipUserData=YES
SkipFinalSummary=YES
FinishAction=SHUTDOWN
DoNotCreateExtraPartition=YES

; Domain Join Configuration
; JoinDomain=$JoinDomain
; DomainAdmin=$DomainAdmin
; DomainAdminDomain=$DomainAdminDomain
; DomainAdminPassword=$DomainAdminPassword
; MachineObjectOU=$MachineObjectOU

; Regional and Locale Settings
TimeZoneName=W. Europe Standard Time
KeyboardLocale=de-DE
UserLocale=de-DE
UILanguage=de-DE

; Display Settings
BitsPerPel=32
VRefresh=60
XResolution=1
YResolution=1

; Hide Windows Shell during deployment
HideShell=NO

; Logging and Monitoring
SLShareDynamicLogging=%DEPLOYROOT%\Logs\%COMPUTERNAME%
"@

# Configure Bootstrap.ini and CustomSettings.ini
# Set Bootstrap.ini and Customsettings.ini properties
Set-Content -Path "$InstallDrive\MDTBuildLab\Control\Bootstrap.ini" -value $BSText
Set-Content -Path "$InstallDrive\MDTBuildLab\Control\CustomSettings.ini" -value $CSText

Start-Sleep -Milliseconds 1000
 
# Create optional MDT Media content
# New-Item -Path $InstallDrive\MEDIA001 -ItemType directory
# New-PSDrive -Name "DS002" -PSProvider MDTProvider -Root "$InstallDrive\MDTBuildLab"
# New-Item -Path "DS002:\Media" -enable "True" -Name "MEDIA001" -Comments "" -Root "$InstallDrive\MEDIA001" -SelectionProfile "Nothing" -SupportX86 "False" -SupportX64 "True" -GenerateISO "False" -ISOName "LiteTouchMedia.iso" -Force -Verbose
# New-PSDrive -Name "MEDIA001" -PSProvider "MDTProvider" -Root "$InstallDrive\MEDIA001\Content\Deploy" -Description "MDT Production Media" -Force -Verbose
 
# Update the MDT Media (and another round of creation because of a bug in MDT internal processing)
# Update-MDTMedia -path "DS002:\Media\MEDIA001" -Verbose
# Remove-Item -path "DS002:\Media\MEDIA001" -force -verbose
# New-Item -path "DS002:\Media" -enable "True" -Name "MEDIA001" -Comments "" -Root "$InstallDrive\MEDIA001" -SelectionProfile "Everything" -SupportX86 "False" -SupportX64 "True" -GenerateISO "False" -ISOName "LiteTouchMedia.iso" -Verbose -Force
# New-PSDrive -Name "MEDIA001" -PSProvider "MDTProvider" -Root "$InstallDrive\MEDIA001\Content\Deploy" -Description "MDT Production Media" -Force -Verbose



# Connect to the deployment share using Windows PowerShell
# https://technet.microsoft.com/en-us/itpro/windows/deploy/create-a-windows-10-reference-image
# Import-Module "C:\Program Files\Microsoft Deployment Toolkit\bin\MicrosoftDeploymentToolkit.psd1"

# How to: Enable or Disable a Server Network Protocol (SQL Server PowerShell)
# https://technet.microsoft.com/en-us/library/dd206997(v=sql.105).aspx
[reflection.assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo")
[reflection.assembly]::LoadWithPartialName("Microsoft.SqlServer.SqlWmiManagement")
$smo = 'Microsoft.SqlServer.Management.Smo.'
$wmi = new-object ($smo + 'Wmi.ManagedComputer').
# List the object properties, including the instance names.
$Wmi
# Disable the TCP protocol on the default instance.
# $uri = "ManagedComputer[@Name='$MDTServer']/ ServerInstance[@Name='ADK']/ServerProtocol[@Name='Tcp']"
$uri = "ManagedComputer[@Name='" + (get-item env:\computername).Value + "']/ServerInstance[@Name='ADK']/ServerProtocol[@Name='Tcp']"
$Tcp = $wmi.GetSmoObject($uri)
$Tcp.IsEnabled = $false
$Tcp.Alter()
$Tcp
# Enable the named pipes protocol for the default instance.
$uri = "ManagedComputer[@Name='" + (get-item env:\computername).Value + "']/ServerInstance[@Name='ADK']/ServerProtocol[@Name='Np']"
# $uri = "ManagedComputer[@Name='$MDTServer']/ ServerInstance[@Name='ADK']/ServerProtocol[@Name='Np']"
$Np = $wmi.GetSmoObject($uri)
$Np.IsEnabled = $true
$Np.Alter()
$Np

Start-Sleep -Milliseconds 1000

Start-Process sc -ArgumentList {config sqlbrowser start= auto} -ErrorAction SilentlyContinue
Start-Service "SQL Server Browser" -ErrorAction SilentlyContinue
Stop-Service "SQL Server (ADK)"  -ErrorAction SilentlyContinue
Start-Service "SQL Server (ADK)" -ErrorAction SilentlyContinue

Start-Sleep -Milliseconds 1000

# Add a new Database
New-MDTDatabase -SQLServer $MDTServer -Database "DB-Build-Lab" -Instance "ADK" -SQLShare "MDTBuildLab$" -Path DS001: -Force 

# Add a Role 
Import-Module "C:\Users\scharmer\Desktop\DOWNLOADS (MDT)\MDTDB\MDTDB.psm1" -Force
Connect-MDTDatabase –sqlServer $MDTServer –database "DB-Build-Lab" -instance ADK
New-MDTRole -name "Role-Build-Defaults" -settings @{SkipWizard="YES"; DoCapture="YES"; SkipAdminPassword="YES";SkipApplications="YES";SkipBDDWelcome="YES";SkipBitLocker="YES"; `
            SkipCapture="NO";SkipComputerBackup="YES";SkipComputerName="YES";SkipDomainMembership="YES";SkipFinalSummary="YES";SkipLocaleSelection="YES"; `
            SkipPackageDisplay="YES";SkipProductKey="YES";SkipSummary="YES";SkipTaskSequence="YES";SkipTimeZone="YES";SkipUserData="YES";} -verbose


# Open Port 1433 TCP und 1434 UDP
New-NetFirewallRule -DisplayName "MSSQL TCP 1433" -Direction Inbound -LocalPort 1433 -Protocol TCP -Action Allow
New-NetFirewallRule -DisplayName "MSSQL UDP 1434" -Direction Inbound -LocalPort 1434 -Protocol UDP -Action Allow


# Add a task sequence. The following cmdlets, which add an operating system, driver, and task sequence will all leverage the Windows PowerShell drive:
#Add the driver
# import-mdtdriver -path “DS002:\Out-of-Box Drivers” -SourcePath “D:\Drivers” -ImportDuplicates -Verbose
#Add the task sequence
Import-MDTTaskSequence -path "DS001:\Task Sequences" -Name "Windows 10" -Template "Client.xml" -Comments "This will deploy Windows 10 to all the desks” -ID "10" -Version “1.0” -OperatingSystemPath “DS001:\Operating Systems\Windows 10\x64\Windows 10 Enterprise in x64 install.wim" -FullName "Windows User" -OrgName "Contoso" -HomePage "about:blank" -AdminPassword "Password1!” -Verbose

# Update the Deployment Share
Update-MDTDeploymentShare -path “DS001:” -Compress –Verbose 



 
# Configure Sharing Permissions for the MDT Build Lab deployment share
$DeploymentShare = "MDTBuildLab$"
Grant-SmbShareAccess -Name $DeploymentShare -AccountName "EVERYONE" -AccessRight Change -Force
Revoke-SmbShareAccess -Name $DeploymentShare -AccountName "CREATOR OWNER" -Force






# Installing SQL Server Management Studio unattend
SSMS-Setup-ENU.exe /install /quiet /norestart








# Create the install: Microsoft Visual C++ 2005 SP1 x86
$ApplicationName = "Install - Microsoft Visual C++ 2005 SP1 - x86"
$CommandLine = "vcredist_x86.exe /Q"
$ApplicationSourcePath = "C:\Setup\apps\VC++2005SP1x86"
Import-MDTApplication -Path "DS001:\Applications\Microsoft" -Enable "True" -Name $ApplicationName -ShortName $ApplicationName -Commandline $Commandline -WorkingDirectory ".\Applications\$ApplicationName" -ApplicationSourcePath $ApplicationSourcePath -DestinationFolder $ApplicationName -Verbose

# Create the install: Microsoft Visual C++ 2005 SP1 x64
$ApplicationName = "Install - Microsoft Visual C++ 2005 SP1 - x64"
$CommandLine = "vcredist_x64.exe /Q"
$ApplicationSourcePath = "C:\Setup\apps\VC++2005SP1x64"
Import-MDTApplication -Path "DS001:\Applications\Microsoft" -Enable "True" -Name $ApplicationName -ShortName $ApplicationName -Commandline $Commandline -WorkingDirectory ".\Applications\$ApplicationName" -ApplicationSourcePath $ApplicationSourcePath -DestinationFolder $ApplicationName -Verbose

# Create the install: Microsoft Visual C++ 2008 SP1 x86
$ApplicationName = "Install - Microsoft Visual C++ 2008 SP1 - x86"
$CommandLine = "vcredist_x86.exe /Q"
$ApplicationSourcePath = "C:\Setup\apps\VC++2008SP1x86"
Import-MDTApplication -Path "DS001:\Applications\Microsoft" -Enable "True" -Name $ApplicationName -ShortName $ApplicationName -Commandline $Commandline -WorkingDirectory ".\Applications\$ApplicationName" -ApplicationSourcePath $ApplicationSourcePath -DestinationFolder $ApplicationName -Verbose

# Create the install: Microsoft Visual C++ 2008 SP1 x64
$ApplicationName = "Install - Microsoft Visual C++ 2008 SP1 - x64"
$CommandLine = "vcredist_x64.exe /Q"
$ApplicationSourcePath = "C:\Setup\apps\VC++2008SP1x64"
Import-MDTApplication -Path "DS001:\Applications\Microsoft" -Enable "True" -Name $ApplicationName -ShortName $ApplicationName -Commandline $Commandline -WorkingDirectory ".\Applications\$ApplicationName" -ApplicationSourcePath $ApplicationSourcePath -DestinationFolder $ApplicationName -Verbose

# Create the install: Microsoft Visual C++ 2010 SP1 x86
$ApplicationName = "Install - Microsoft Visual C++ 2010 SP1 - x86"
$CommandLine = "vcredist_x86.exe /Q"
$ApplicationSourcePath = "C:\Setup\apps\VC++2010SP1x86"
Import-MDTApplication -Path "DS001:\Applications\Microsoft" -Enable "True" -Name $ApplicationName -ShortName $ApplicationName -CommandLine $CommandLine -WorkingDirectory ".\Applications\$ApplicationName" -ApplicationSourcePath $ApplicationSourcePath -DestinationFolder $ApplicationName -Verbose

# Create the install: Microsoft Visual C++ 2010 SP1 x64
$ApplicationName = "Install - Microsoft Visual C++ 2010 SP1 - x64"
$CommandLine = "vcredist_x64.exe /Q"
$ApplicationSourcePath = "C:\Setup\apps\VC++2010SP1x64"
Import-MDTApplication -Path "DS001:\Applications\Microsoft" -Enable "True" -Name $ApplicationName -ShortName $ApplicationName -CommandLine $CommandLine -WorkingDirectory ".\Applications\$ApplicationName" -ApplicationSourcePath $ApplicationSourcePath -DestinationFolder $ApplicationName -Verbose

# Create the install: Microsoft Visual C++ 2015 x86
$ApplicationName = "Install - Microsoft Visual C++ 2015 - x86"
$CommandLine = "vcredist_x86.exe /Q"
$ApplicationSourcePath = "C:\Setup\apps\VC++2015Ux86"
Import-MDTApplication -Path "DS001:\Applications\Microsoft" -Enable "True" -Name $ApplicationName -ShortName $ApplicationName -CommandLine $CommandLine -WorkingDirectory ".\Applications\$ApplicationName" -ApplicationSourcePath $ApplicationSourcePath -DestinationFolder $ApplicationName -Verbose

# Create the install: Microsoft Visual C++ 2015 x64
$ApplicationName = "Install - Microsoft Visual C++ 2015 - x64"
$CommandLine = "vcredist_x64.exe /Q"
$ApplicationSourcePath = "C:\Setup\apps\VC++2015Ux64"
Import-MDTApplication -Path "DS001:\Applications\Microsoft" -Enable "True" -Name $ApplicationName -ShortName $ApplicationName -CommandLine $CommandLine -WorkingDirectory ".\Applications\$ApplicationName" -ApplicationSourcePath $ApplicationSourcePath -DestinationFolder $ApplicationName -Verbose






 # Configure Windows Deployment Services
 WDSUTIL /Verbose /Progress /Initialize-Server /Server:SRV1 /RemInst:"C:\RemoteInstall"
 WDSUTIL /Set-Server /AnswerClients:All
 # Use WDSUTIL to update images
 wdsutil /Verbose /Progress /Add-Image /ImageFile:C:\DeploymentShare\Boot\LiteTouchPE_x64.wim /ImageType:Boot






New-VM –Name "PC2" –NewVHDPath "c:\vhd\pc2.vhdx" -NewVHDSizeBytes 60GB -SwitchName poc-internal -BootDevice NetworkAdapter -Generation 2
Set-VMProcessor -VMName "PC001" -Count 2
# Set-VMMemory -VMName "PC001" -DynamicMemoryEnabled $true -MinimumBytes 720MB -MaximumBytes 2048MB -Buffer 20
Start-VM PC2
vmconnect localhost PC2







