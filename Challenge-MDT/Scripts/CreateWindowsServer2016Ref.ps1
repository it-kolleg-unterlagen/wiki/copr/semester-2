﻿## Parent Script by http://deploymentresearch.com/Research/Post/578/Building-the-perfect-Windows-Server-2016-reference-image
## <!-- Modified by Alexander Scharmer (SALE) 12.02.2017 !>


# Check for elevation
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    Write-Warning "Oupps, you need to run this script from an elevated PowerShell prompt!`nPlease start the PowerShell prompt as an Administrator and re-run the script."
    Write-Warning "Aborting script..."
    Throw
}


$ISO = "D:\alexhome\Server\ISO\SW_DVD9_Win_Svr_STD_Core_and_DataCtr_Core_2016_64Bit_German_-2_MLF_X21-22827.ISO"
$ServiceStack = "D:\alexhome\Server\MSU\windows10.0-kb4013418-x64_b7165b95be791aeb025135efe60e00db2800c5c6.msu"
$Latestdefinitionupdates = "D:\alexhome\Server\MSU\windows10.0-kb3150513-x64_e2a93cf2595104dbdb737fcbd73feddecf224fd4.msu"
$CU = "D:\alexhome\Server\MSU\windows10.0-kb4015217-x64_60bfcc7b365f9ab40608e2fb96bc2be8229bc319.msu"
$MountFolder = "D:\alexhome\Server\Mount"
$RefImage = "D:\alexhome\Server\WIM\install.wim"
 
# Verify that the ISO and CU files existnote
if (!(Test-Path -path $ISO)) {Write-Warning "Could not find Windows Server 2016 ISO file. Aborting...";Break}
if (!(Test-Path -path $CU)) {Write-Warning "Cumulative Update for Windows Server 2016. Aborting...";Break}
# Delete RefImage-File before continue.
if (Test-Path -path $RefImage) {Write-Warning "RefImage exists. Delete it.";Remove-Item $RefImage -Force}
 
# Mount the Windows Server 2016 ISO
Mount-DiskImage -ImagePath $ISO
$ISOImage = Get-DiskImage -ImagePath $ISO | Get-Volume
$ISODrive = [string]$ISOImage.DriveLetter+":"
 
# Extract the Windows Server 2016 Standard index to a new WIM
Export-WindowsImage -SourceImagePath "$ISODrive\Sources\install.wim" -SourceName "Windows Server 2016 SERVERDATACENTER" -DestinationImagePath $RefImage


# Add .NET Framework 3.5.1 to the Windows Server 2016 Standard image
Add-WindowsPackage -PackagePath $ISODrive\sources\sxs\microsoft-windows-netfx3-ondemand-package.cab -Path $MountFolder
# Enable-WindowsOptionalFeature -FeatureName NetFx3ServerFeatures -Path D:\Mount-All -All

# install drivers
# Add-WindowsDriver –Path $MountFolder –Driver C:\server\Drivers –Recurse   # installs drivers in specified directory and subdirectories

 
# Add the Cumulative Update for Windows Server 2016 for x64-based Systems (KB4015217) to the Windows Server 2016 Standardimage
if (!(Test-Path -path $MountFolder)) {New-Item -path $MountFolder -ItemType Directory}
Mount-WindowsImage -ImagePath $RefImage -Index 1 -Path $MountFolder
Add-WindowsPackage -PackagePath $ServiceStack -Path $MountFolder 
Add-WindowsPackage -PackagePath $CU -Path $MountFolder

 

# Settings to apply
# $RemoteDesktopEnable = $True # Enabling Remote Desktop

# $hive  = Start-Process -FilePath "REG.EXE" -ArgumentList @("LOAD HKLM\REMOTE", (Join-Path $MountFolder "Windows\System32\Config\System")) -NoNewWindow
#             Start-Sleep -s 3        
#                         if ( $RemoteDesktopEnable -eq $True ) {
# 
#                           
#                             Set-ItemProperty -Path "HKLC:\REMOTE\ControlSet001\Control\Terminal Server" -Name "fDenyTSConnections" -Value 0
#                     }
# 
# 
#           Start-Sleep -s 2
#          Start-Process -FilePath "REG.EXE" -ArgumentList @("UNLOAD HKLM\REMOTE") -NoNewWindow

 
# Dismount the Windows Server 2016 Standard image
DisMount-WindowsImage -Path $MountFolder -save # -CheckIntegrity or -Discard
#DisMount-WindowsImage -Path $MountFolder -Discard

 
# Dismount the Windows Server 2016 ISO
Dismount-DiskImage -ImagePath $ISO -Verbose


# Create a bootable ISO. The script assumes you installed Windows ADK 10 in the default location.

$DesktopPath = [Environment]::GetFolderPath("Desktop")  
$MediaFolder = 'D:\alexhome\Server\Media'
$ISOFile = "$DesktopPath\Windows2016Build1066.iso"
$ToolsPath = "C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg"

# replace modified wim file within media folder
Copy-Item -Path $RefImage -Destination "$MediaFolder\sources" -Force -PassThru

 
Write-Output "Checking for oscdimg.exe"
If (Test-Path $ToolsPath\oscdimg.exe){
    Write-Output "Oscdimg.exe found, OK, continuing..."
    Write-Output ""
    }
Else {
    Write-Warning "Oupps, cannot find Oscdimg.exe. Make sure it is copied to $ToolsPath. Aborting"
    Break
}
 
Write-Output "Checking for Media folder"
If (Test-Path $MediaFolder\setup.exe){
    Write-Output "Installation media found, OK, continuing..."
    Write-Output ""
    }
Else {
    Write-Warning "Oupps, cannot find Installation media. Make sure it is copied to $MediaFolder. Aborting"
    Break
}
 
# Write-Output "Checking for ISO folder"
# If (Test-Path $ISO){
 #   Write-Output "ISO folder found, OK, continuing..."
 #   Write-Output "
 #   }
#Else {
 #   Write-Warning "ISO folder not found, creating $ISO folder"
 #   New-Item $ISO -ItemType Directory
#}
 
  
# Create the Windows ISO
$BootData='2#p0,e,b"{0}"#pEF,e,b"{1}"' -f "$ToolsPath\etfsboot.com","$ToolsPath\efisys_noprompt.bin"
  
$Proc = Start-Process -FilePath "$ToolsPath\oscdimg.exe" -ArgumentList @("-bootdata:$BootData",'-u2','-udfver102',"$MediaFolder","$ISOFile") -PassThru -Wait -NoNewWindow
if($Proc.ExitCode -ne 0)
{
    Throw "Failed to generate ISO with exitcode: $($Proc.ExitCode)"
}
else {


    Write-Host "Media creation successfully done!" -ForegroundColor Green


}