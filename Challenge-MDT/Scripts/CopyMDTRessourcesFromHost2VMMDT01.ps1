﻿# CopyMDTRessourcesFromHost2VMMDT01.ps1
# https://technet.microsoft.com/en-us/itpro/powershell/windows/hyper-v/copy-vmfile
# .NET-Library um Dateien und Ordner zu zippen.
[Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem")
$Compression = [System.IO.Compression.CompressionLevel]::Optimal
$IncludeBaseDirectory = $false

# Pfade anpassen
# Ordner Source angeben - sämtliche Inhalte hierin werden komprimiert.
$Source = "C:\MDTSources"

# Pfad und Dateiangabe der ZIP-Datei.
$Destination = "C:\MDT\MDTSources.zip"
New-Item -Path C:\MDT -ItemType Directory -Force -ErrorAction SilentlyContinue
Remove-Item -Path $Destination -Force -ErrorAction SilentlyContinue

# Los - zippe!
[System.IO.Compression.ZipFile]::CreateFromDirectory($Source,$Destination,$Compression,$IncludeBaseDirectory)

# Warte kurz ...
Start-Sleep -Milliseconds 1000

# ... und kopiere die ZIP-Datei in die virtuelle Maschine (die Aktivierung der Gastdienste ist erforderlich).
Copy-VMFile MDT01 -SourcePath $Destination -DestinationPath "C:\MDT\MDTSources.zip" -CreateFullPath -FileSource Host
\end{tcblisting}
\label{lst:ZipCopy2VM.ps1}

Stellen Sie sicher, bevor Sie das Script \textit{ZipCopy2VM.ps1} nutzen, dass die Integrationsdienste \textit{Gastdienste} in der Hyper-V-Instanz \textit{MDT01} aktiviert sein müssen. Aktiviert wird dies in der grafischen Oberfläche (siehe \vref{fig:mdt01-06}) oder via PowerShell:

\begin{tcblisting}{listing only,title=Gastdienstschnittstelle in VM MDT01 aktivieren,listing options={language=bash,columns=fullflexible,keywordstyle=\color{red}}}
Enable-VMIntegrationService "Gastdienstschnittstelle" -VMName MDT01