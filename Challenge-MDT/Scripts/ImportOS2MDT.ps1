﻿# ImportOS2MDT.ps1
Add-PSSnapIn Microsoft.BDD.PSSnapIn -ErrorAction SilentlyContinue 
New-PSDrive -Name "DS001" -PSProvider MDTProvider `
	-Root "$InstallDrive\MDTProdLab" `
	-Description "MDT ProdLab" `
	-NetworkPath "\\$MDTServer\MDTProdLab$"
Import-MDTOperatingSystem -Path "DS001:\Operating Systems" `
	-SourcePath "D:\" -DestinationFolder "W10x64-EDU" -Verbose