﻿# New virtual Switch 
New-VMSwitch –SwitchName "vSwitchNAT” –SwitchType Internal 

# Set Gateway IPv4 for vm-machines 
# Port-Kategorien
New-NetIPAddress –IPAddress 10.0.0.254 -PrefixLength 24 -InterfaceAlias "vEthernet (vSwitchNAT)" 

# Activate NAT
New-NetNat –Name "NATNetwork" –InternalIPInterfaceAddressPrefix "10.0.0.0/24"


<# ----
# To forward specific ports from the Host to the guest VMs you can use the following commands.
# This example creates a mapping between port 80 of the host to port 80 of a Virtual Machine with an IP address of 172.21.21.2.
Add-NetNatStaticMapping -NatName “VMSwitchNat” -Protocol TCP -ExternalIPAddress 0.0.0.0 -InternalIPAddress 172.21.21.2 -InternalPort 80 -ExternalPort 80

#This example creates a mapping between port 82 of the Virtual Machine host to port 80 of a Virtual Machine with an IP address of 172.21.21.3.
Add-NetNatStaticMapping -NatName “VMSwitchNat” -Protocol TCP -ExternalIPAddress 0.0.0.0 -InternalIPAddress 172.16.0.3 -InternalPort 80 -ExternalPort 82
#>

