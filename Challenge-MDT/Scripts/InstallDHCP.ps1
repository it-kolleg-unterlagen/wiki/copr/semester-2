﻿# InstallDHCP.ps1
# Installiert die Rolle DHCP-Server mit Verwaltungswerkzeugen 
Install-WindowsFeature -Name DHCP -IncludeManagementTools
# Erstellt die DHCP-Sicherheitsgruppen
# https://technet.microsoft.com/de-de/library/ee941205(v=ws.10).aspx
Netsh dhcp add securitygroups
# Startet den DHCP-Dienst für die Initialisierung von DHCP neu
Restart-Service DHCPServer -Confirm:$false
# Autorisiert den DHCP-Dienst in Active Directory
Add-DhcpServerInDC  dc01.contoso.int  10.0.0.1
# Setzt den Installationsstatus der DHCP-Konfig auf erledigt
Set-ItemProperty –Path 
   registry::HKEY_LOCAL_MACHINE\SOFTWARE\
       Microsoft\ServerManager\Roles\12 `
   –Name ConfigurationState `
   –Value 2
# DHCP-Bereich erstellen
Add-DhcpServerv4Scope -Name "MDT-Machbarkeitsstudie" `
	-StartRange 10.0.0.100 `
	-EndRange 10.0.0.199 `
	-SubnetMask 255.255.255.0 `
	-Description "Windows 10 Machbarkeitsstudie" `
	-State Active
# Optionen für den DHCP-Bereich definieren
Set-DhcpServerv4OptionValue `
	-ScopeId 10.0.0.0 `
	-DnsDomain contoso.int `
	-DnsServer 10.0.0.1 `
	-Force
	
